﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KuusteistJaan
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Anna üks arv: ");
                int arv = int.Parse(Console.ReadLine());
                Console.WriteLine("Anna teine arv: ");
                int arv2 = int.Parse(Console.ReadLine());
                Console.WriteLine(arv / arv2);
            }
            catch (FormatException a)
            {
                
                Console.WriteLine(a.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Mingi jama ({e.Message})");

            }
            finally
            {
                Console.WriteLine("Pulmalaud tuleb ikka kinni maksta");
            }

            Console.WriteLine("Kõik hästi");

        }
    }
}
